import { connect, StringCodec } from "nats";



const queue = await connect({ servers: "localhost:4222" });


const sc = StringCodec();


const publisher = async(msg) => {
    return await queue
        .request("hello", sc.encode(msg), { timeout: 2000 })
        .then((m) => {
            return sc.decode(m.data);
        })
        .catch(() => {
            return null;
        });
};

export default {
    publisher,
};