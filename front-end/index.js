import express from "express";
import queue from "./queue.js";
const app = express();
const PORT = process.env.PORT || 8080;


app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get("/", async(req, res) => {
    res.status(200).send({
        message: 'GET Home route working fine!'
    });
});

app.post('/post', async(req, res) => {
    const user = req.body.user;
    const result = await queue.publisher(JSON.stringify(user));
    console.log(user);
    res.send("Peticion del usuario: " + result);
});

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});