import { connect, StringCodec } from "nats";



const queue = await connect({ servers: "localhost:4222" });


const sc = StringCodec();

const sub = queue.subscribe("hello");
(async() => {
    for await (const m of sub) {
        const text = JSON.parse(sc.decode(m.data));
        console.log(`[${sub.getProcessed()}]: ${sc.decode(m.data)}`);
        var result = echo(text);
        m.respond(sc.encode(result));
    }
    console.log("subscription closed");
})();

function echo(text) {
    return text;
}